﻿using System;
using System.Globalization;

namespace _1_CultureInfo
{
    // CultureInfo Class
    // https://msdn.microsoft.com/en-us/library/system.globalization.cultureinfo(vs.71).aspx

    // Standard Date and Time Format Strings
    // c
    class Program
    {
        static void Main(string[] args)
        {
            // Get Info about current culture
            CultureInfo currentCulture = CultureInfo.CurrentCulture;
            Console.WriteLine("Current Culture: {0}.", currentCulture);

            // Get information about all cultures in system
            CultureInfo[] cultureInfo = CultureInfo.GetCultures(CultureTypes.NeutralCultures);
            Console.WriteLine("{0} different cultures in the system.", cultureInfo.Length);

            //foreach (CultureInfo ci in cultureInfo)
            //{
            //    Console.WriteLine(ci.EnglishName + " | " + ci.ToString());
            //}

            //var specificCultures = new List<string>
            //{
            //    "hy-AM",
            //    "ru-RU",
            //    "en-US",
            //    "en-US",
            //    "ar-AE"
            //};

            string text = DateTime.Now.ToString("D");

            const string cn = "ru-RU";
            string cnAm = "hy-AM";
            var ci1 = new CultureInfo("hy-AM");
            string info = DateTime.Now.ToString("D", ci1);
            string info1 = string.Format("{0} {1}", ci1.NumberFormat.CurrencySymbol, 235.6);
            string info2 = 235.6.ToString("C", ci1);

            string ssss = DateTime.Now.ToString("yy - MMMM d", ci1);

            // Delay.
            Console.ReadKey();
        }

        public enum RateType
        {
            amd = 1,
            usd,
            eur,
            rub,
            /// <summary>
            /// <para>Arab Emirates Dirham</para>
            /// <para>United Arab Emirates</para>
            /// </summary>
            aed
        }
    }
}
