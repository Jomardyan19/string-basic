﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_StringBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            string st = "A1" + "A2" + "A3";
            //A1
            //A2
            //A1A2
            //A3
            //A1A2A3

            MyBuilder myBuilder = new MyBuilder();
            myBuilder
                .Append(10)
                .Append(20)
                .Append(30);

            string strt1 = @"C:\inetpub\custerr\en-US";
            Console.WriteLine(strt1);
            
            string str1 = "Aaa";
            string str2 = "Aaa";
            if(str1.Equals(str2))
            {

            }

            if(object.ReferenceEquals(str1, str2))
            {

            }
                        
            string str = "StringBuilder " + "is " + "very " + "fast ... ";
            // StringBuilder                    -> 0x123
            // StringBuilder is                 -> 0x124
            // StringBuilder is very            -> 0x135
            // StringBuilder is very fast ...   -> 0x136

            string st1 = "A1";
            string st2 = "A1";
            if (object.ReferenceEquals(st1, st2))
            {

            }

            var builder = new StringBuilder();

            builder
                .Append("StringBuilder ")
                .Append("is ")
                .Append("is ")
                .Append("very ")
                .Append("fast ... ");

            

            builder.Append("StringBuilder ");
            builder.Append("is ");
            builder.Append("is ");

            string build1 = builder.ToString();

            builder.Append("faster");

            string build2 = builder.ToString();

            Console.WriteLine(build1);
            Console.WriteLine(build2);

            // Delay.
            Console.ReadKey();
        }
    }

    class MyBuilder
    {
        public MyBuilder()
        {
            _list = new List<int>();
        }

        private List<int> _list;

        public MyBuilder Append(int value)
        {
            _list.Add(value);
            return this;
        }
    }
}