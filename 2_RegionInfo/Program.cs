﻿using System;
using System.Globalization;

namespace _2_RegionInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            var ri = new RegionInfo("hy-AM");

            // Get info about current region
            RegionInfo regionInfo = RegionInfo.CurrentRegion;
            Console.WriteLine("EnglishName:\t{0}.", regionInfo.EnglishName);
            Console.WriteLine("NativeName:\t{0}.", regionInfo.NativeName);

            Console.WriteLine(new string('-', 35));

            Console.WriteLine("CurrencySymbol:\t{0}", regionInfo.CurrencySymbol);
            Console.WriteLine("CurrencyEnglishName:\t{0}", regionInfo.CurrencyEnglishName);
            Console.WriteLine("CurrencyNativeName:\t{0}", regionInfo.CurrencyNativeName);

            Console.WriteLine(new string('-', 35));

            // Получение информации о текущем формате даты: названия дней.
            string[] days = CultureInfo.CurrentCulture.DateTimeFormat.DayNames;

            Console.WriteLine("Дни недели:");
            foreach (string day in days)
            {
                Console.WriteLine(day);
            }

            Console.WriteLine(new string('-', 35));

            // Получение информации о формате даты в армянском языке: названия дней.
            days = CultureInfo.GetCultureInfo("hy-AM").DateTimeFormat.DayNames;

            Console.WriteLine("Дни недели на немецком:");
            foreach (string day in days)
            {
                Console.WriteLine(day);
            }

            decimal money = 156.7569M;
            string cur = money.ToString("C", CultureInfo.GetCultureInfo("hy-AM"));

            // Delay.
            Console.ReadKey();
        }
    }
}