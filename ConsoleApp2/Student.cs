﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Student
    {
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            Student st = obj as Student;
            if (st == null)
                return false;

            return this.Name == st.Name;
        }
    }
}